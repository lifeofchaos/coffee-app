import "coffee_mug/dist/webcomponentsloader";
import "coffee_mug/dist/coffee";
import "./styles.css";
import "normalize.css";

const axios = require('axios');

const loader = document.querySelector('#overlay');
const coffeeGrid = document.querySelector('.coffee_grid');
const closeModal = document.querySelector('.cerrar_modal');
const coffeeModal = document.querySelector('.coffee_details');

const modalCoffeeName = document.querySelector('#coffeeName');
const modalCoffeeComponent = document.querySelector('.modal_coffee');
const modalCoffeeDescription = document.querySelector('.modalDescription');

closeModal.addEventListener('click', (e) => {
    e.preventDefault();
    coffeeModal.classList.remove('visible');
    resetCoffee();
});

function resetCoffee() {
    [...modalCoffeeComponent.attributes].forEach(attr => {
        if (attr.name !== 'class' || attr.name.includes('-color')) {
            // Reseteamos todo a 0
            modalCoffeeComponent.setAttribute(attr.name, 0);
        }
    });
}

function getCoffeeInfo(e) {
    e.preventDefault();
    let coffee = e.currentTarget.getAttribute('data-coffee');
    loader.style.display = 'flex';
    axios.get(`https://coffee.lifeofchaos.com/api/coffee/${coffee}`).then(response => {
        response = response.data;
        if (response.status === true) {
            let coffee = response.coffee;
            for (let prop of Object.keys(coffee)) {
                if (prop === 'name') {
                    modalCoffeeName.textContent = coffee[prop];
                } else if (prop === 'description') {
                    modalCoffeeDescription.innerHTML = coffee[prop];
                } else {
                    modalCoffeeComponent.setAttribute(prop, coffee[prop]);
                }
            }
            coffeeModal.classList.add('visible');
        } else {
            alert('ERROR');
        }
    }).finally(() => {
        loader.style.display = 'none';
    });
}

axios.get('https://coffee.lifeofchaos.com/api/list').then(response => {
    response = response.data;
    if (response.status === true) {
        let coffeeList = response.coffeeList;
        for (let coffee of Object.keys(coffeeList)) {
            let current = coffeeList[coffee];

            let currentCoffeeDOM = document.createElement('div');
            currentCoffeeDOM.classList.add('coffee_el');
            currentCoffeeDOM.setAttribute('data-coffee', coffee);
            currentCoffeeDOM.addEventListener('click', getCoffeeInfo);
            let nameTag = document.createElement('p');
            nameTag.classList.add('coffee_name');

            let coffeeDom = document.createElement('loc-coffee');
            coffeeDom.setAttribute('mug-color', '#cfd2d6');
            coffeeDom.setAttribute('dish-color', '#cfd2d6');
            for (let prop of Object.keys(current)) {
                if (prop === 'name') {
                    nameTag.textContent = current[prop];
                } else {
                    coffeeDom.setAttribute(prop, current[prop]);
                }
            }
            currentCoffeeDOM.appendChild(coffeeDom);
            currentCoffeeDOM.appendChild(nameTag);
            coffeeGrid.appendChild(currentCoffeeDOM);
        }
    }
}).finally(() => {
    setTimeout(() => {
        loader.style.display = 'none';
    }, 800);
});